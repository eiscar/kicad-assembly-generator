# KiCad PCB pick and place assistant
This script generates a pdf with component footprints and references to help assembling PCB boards. 

# Usage 
Call the script with the path to the .kicad_pcb layout file as an argument. By default it will only generate a pdf for components located on the front layer. 
If you want to plot components on the back layer, pass it the "--layer B_Cu" option after the path to the board. 

# Source: 
This script is a modification of the work of Github user [pwuertz](https://gist.github.com/pwuertz/152f15b2dadca9a74039aeab944714af)